package ru.nsu.fit.g16202.korshunov.mvp.model.volume;

import javafx.util.Pair;
import ru.nsu.fit.g16202.korshunov.mvp.model.Model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EmissionPlot {

    private List<Double> rValues = new ArrayList<>(101);
    private List<Double> gValues = new ArrayList<>(101);
    private List<Double> bValues = new ArrayList<>(101);

    private List<Pair<Point,Point>> doublePointsR = new ArrayList<>();
    private List<Pair<Point,Point>> doublePointsG = new ArrayList<>();
    private List<Pair<Point,Point>> doublePointsB = new ArrayList<>();

    EmissionPlot(Scanner scanner) throws IOException {
        loadPlot(scanner);
    }

    private void loadPlot(Scanner s) throws IOException {
        try {
            String num = s.nextLine();
            int n = Integer.valueOf(num);
            for (int i = 0; i < n; ++i) {
                String[] str = s.nextLine().split(" ");
                int coord = Integer.valueOf(str[0]);
                double val = Double.valueOf(str[1]);
                interpolatePlot(rValues, coord, val, doublePointsR);
                val = Double.valueOf(str[2]);
                interpolatePlot(gValues, coord, val, doublePointsG);
                val = Double.valueOf(str[3]);
                interpolatePlot(bValues, coord, val, doublePointsB);
            }
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException ex) {
            throw new IOException();
        }
    }

    @SuppressWarnings("Duplicates")
    private void interpolatePlot(List<Double> values, int coord, double val, List<Pair<Point, Point>> doublePoints) {
        if (values.isEmpty()) {
            for (int a = 0; a <= coord; a++) {
                values.add(val);
            }
        } else if (coord != values.size() - 1) {
            double last = values.get(values.size() - 1);
            double step = (val - last) / (coord - values.size() + 1);
            for (int a = values.size(); a < coord; a++) {
                last += step;
                values.add(last);
            }
            values.add(val);
        } else {
            doublePoints.add(new Pair<>(new Point(coord, values.get(values.size()-1)), new Point(coord, val)));
            values.remove(values.size() - 1);
            values.add(val);
        }
    }

    public double getValue(double coordinate, Model.ColorComponent component) {
        int l = (int) coordinate;
        int r = l + 1;
        double dif = 0.0;
        double val = 0.0;
        switch (component) {
            case RED:
                val = rValues.get(l);
                if (l == 100) return val;
                dif = rValues.get(r) - val;
                break;
            case GREEN:
                val = gValues.get(l);
                if (l == 100) return val;
                dif = gValues.get(r) - val;
                break;
            case BLUE:
                val = bValues.get(l);
                if (l == 100) return val;
                dif = bValues.get(r) - val;
                break;
        }
        return val + dif * (coordinate-l);
    }

    public List<Pair<Point, Point>> getDoublePointsR() {
        return doublePointsR;
    }

    public List<Pair<Point, Point>> getDoublePointsG() {
        return doublePointsG;
    }

    public List<Pair<Point, Point>> getDoublePointsB() {
        return doublePointsB;
    }
}
