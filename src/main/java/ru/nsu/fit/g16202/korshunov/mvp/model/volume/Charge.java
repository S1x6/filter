package ru.nsu.fit.g16202.korshunov.mvp.model.volume;

public class Charge {
    private double x;
    private double y;
    private double z;
    private double q;

    public Charge(String x, String y, String z, String q) {
        this.x = Double.valueOf(x) * 350;
        this.y = Double.valueOf(y) * 350;
        this.z = Double.valueOf(z) * 350;
        this.q = Double.valueOf(q);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getQ() {
        return q;
    }

    public void setQ(double q) {
        this.q = q;
    }
}
