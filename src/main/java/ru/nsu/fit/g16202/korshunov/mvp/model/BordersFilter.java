package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.awt.image.BufferedImage;

import static ru.nsu.fit.g16202.korshunov.mvp.model.Model.ColorComponent.BLUE;

public class BordersFilter {

    private Model model;

    public BordersFilter(Model model) {
        this.model = model;
    }

    void applyRobertsFilter(BufferedImage srcImage, BufferedImage destImage, int parameter) {
        model.applyGrayScaleFilter(srcImage, destImage);
        applyGradient(destImage, parameter);
    }

    private int getG1(BufferedImage image, int x, int y) {
        int y2 = x + 1 >= image.getWidth() || y + 1 >= image.getHeight() ? 0 :
                image.getRGB(x + 1, y + 1) & 0x0000ff;
        int y1 = image.getRGB(x, y) & 0x0000ff;
        return y1 - y2;
    }

    private int getG2(BufferedImage image, int x, int y) {
        int y2 = y + 1 >= image.getHeight() ? 0 :
                image.getRGB(x, y + 1) & 0x0000ff;
        int y1 = x + 1 >= image.getWidth() ? 0 : image.getRGB(x + 1, y) & 0x0000ff;
        return y1 - y2;
    }

    private void applyGradient(BufferedImage image, int parameter) {
        int white = Color.WHITE.getRGB();
        for (int j = 0; j < image.getHeight(); ++j) { //для каждого пикселя
            for (int i = 0; i < image.getWidth(); ++i) {
                int G1 = getG1(image, i, j);
                int G2 = getG2(image, i, j);
//                int F = Math.abs(G1) - Math.abs(G2);
                int F = (int) (Math.sqrt(G1 * G1 + G2 * G2));
                if (F < parameter) {
                    image.setRGB(i, j, 0);
                } else {
                    image.setRGB(i, j, white);
                }
            }
        }
    }

    public void applySobelFilter(BufferedImage srcImage, BufferedImage destImage, int parameter) {
        BufferedImage image = new BufferedImage(350,350, BufferedImage.TYPE_INT_RGB);
        model.applyGrayScaleFilter(srcImage, image);
        applySobelGradient(image, destImage, parameter, BLUE);
    }

    private int getS1(BufferedImage image, int x, int y, Model.ColorComponent component) {
        int[][] matrix = {
                {-1, 0, 1},
                {-2, 0, 2},
                {-1, 0, 1}
        };
        int a = ModelUtils.applyMatrix(image, x, y, matrix, component);
        return a;
    }

    private int getS2(BufferedImage image, int x, int y, Model.ColorComponent component) {
        int[][] matrix = new int[3][3];
        matrix[0][0] = -1;
        matrix[0][1] = -2;
        matrix[0][2] = -1;
        matrix[1][0] = 0;
        matrix[1][1] = 0;
        matrix[1][2] = 0;
        matrix[2][0] = 1;
        matrix[2][1] = 2;
        matrix[2][2] = 1;
        return ModelUtils.applyMatrix(image, x, y, matrix, component);
    }

    private void applySobelGradient(BufferedImage srcImage, BufferedImage destImage, int parameter, Model.ColorComponent component) {
        int white = Color.WHITE.getRGB();
        for (int j = 0; j < srcImage.getHeight(); ++j) { //для каждого пикселя
            for (int i = 0; i < srcImage.getWidth(); ++i) {
                int S1 = getS1(srcImage, i, j, component);
                int S2 = getS2(srcImage, i, j, component);
//                int F = Math.abs(S1) + Math.abs(S2);
                int F = (int) (Math.sqrt(S1 * S1 + S2 * S2));
                if (F < parameter) {
                    destImage.setRGB(i,j,0);
                } else {
                    destImage.setRGB(i,j,white);
                }
            }
        }
    }
}
