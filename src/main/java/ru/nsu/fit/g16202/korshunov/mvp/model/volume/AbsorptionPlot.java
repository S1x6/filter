package ru.nsu.fit.g16202.korshunov.mvp.model.volume;

import javafx.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@SuppressWarnings("Duplicates")
public class AbsorptionPlot {

    private List<Double> values = new ArrayList<>(101);
    private List<Pair<Point, Point>> doublePoints = new ArrayList<>();

    AbsorptionPlot(Scanner scanner) throws IOException {
        loadPlot(scanner);
    }

    public double getValue(double coordinate) {
        int l = (int) coordinate;
        int r = l + 1;
        double dif;
        double val;
        val = values.get(l);
        if (l == 100) return val;
        dif = values.get(r) - val;
        return val + dif * (coordinate-l);
    }

    private void loadPlot(Scanner s) throws IOException {
        try {
            String num = s.nextLine();
            int n = Integer.valueOf(num);
            for (int i = 0; i < n; ++i) {
                String[] str = s.nextLine().split(" ");
                int coord = Integer.valueOf(str[0]);
                double val = Double.valueOf(str[1]);
                if (values.isEmpty()) {
                    for (int a = 0; a <= coord; a++) {
                        values.add(val);
                    }
                } else if (coord != values.size()-1){
                    double last = values.get(values.size() - 1);
                    double step = (val - last) / (coord - values.size()+1);
                    for (int a = values.size(); a < coord; a++) {
                        last += step;
                        values.add(last);
                    }
                    values.add(val);
                } else {
                    doublePoints.add(new Pair<>(new Point(coord, values.get(values.size()-1)), new Point(coord, val)));
                    values.remove(values.size()-1);
                    values.add(val);
                }
            }
        } catch (NumberFormatException ex) {
            throw new IOException();
        }
    }

    public List<Pair<Point, Point>> getDoublePoints() {
        return doublePoints;
    }
}
