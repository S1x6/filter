package ru.nsu.fit.g16202.korshunov.mvp.model.volume;

import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.model.ModelUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class VolumeVisualization {

    private static AbsorptionPlot aPlot;
    private static EmissionPlot ePlot;
    private static List<Charge> chargeList;
    private double minQ;
    private double maxQ;

    public VolumeVisualization() {
    }

    public static void loadConfig(File file) throws IOException {
        Scanner scanner = new Scanner(file);
        aPlot = new AbsorptionPlot(scanner);
        ePlot = new EmissionPlot(scanner);
        loadCharges(scanner);
        System.out.println("ok");
    }

    private static void loadCharges(Scanner scanner) {
        chargeList = new ArrayList<>();
        int num = Integer.valueOf(scanner.nextLine());
        for (int i = 0; i < num; i++) {
            String[] s = scanner.nextLine().split(" ");
            chargeList.add(new Charge(s[0], s[1], s[2], s[3]));
        }
    }

    public static AbsorptionPlot getAbsorptionPlot() {
        return aPlot;
    }

    public static EmissionPlot getEmissionPlot() {
        return ePlot;
    }

    private double getDistance(double x, double y, double z, Charge q) {
        return Math.sqrt((q.getX() / 350 - x / 350) * (q.getX() / 350 - x / 350) + (q.getY() / 350 - y / 350) * (q.getY() / 350 - y / 350) + (q.getZ() / 350 - z / 350) * (q.getZ() / 350 - z / 350));
    }

    private double getChargeSum(double x, double y, double z) {
        double q = 0.0;
        for (int i = 0; i < chargeList.size(); i++) {
            double dist = getDistance(x, y, z, chargeList.get(i));
            if (dist <= 0.1) {
                q += chargeList.get(i).getQ() / 0.1;
            } else {
                q += chargeList.get(i).getQ() / dist;
            }
        }
        return q;
    }

    void setVoxels(double[][][] voxels) {
        minQ = getChargeSum(0, 0, 0);
        maxQ = getChargeSum(0, 0, 0);
        for (int i = 0; i < 350; i++) {
            for (int j = 0; j < 350; j++) {
                for (int k = 0; k < 350; k++) {
                    voxels[i][j][k] = getChargeSum(i, j, k);
                    if (voxels[i][j][k] > maxQ) maxQ = voxels[i][j][k];
                    if (voxels[i][j][k] < minQ) minQ = voxels[i][j][k];
                }
            }
        }
    }

    private double getNormalizedCoordinate(double q) {
        q -= minQ;
        return q / (maxQ - minQ) * 100;
    }

    private int processRay(int x, int y, int color, boolean absorption, boolean emission, double[][][] voxels) {
        double r = ModelUtils.getComponent(color, Model.ColorComponent.RED);
        double g = ModelUtils.getComponent(color, Model.ColorComponent.GREEN);
        double b = ModelUtils.getComponent(color, Model.ColorComponent.BLUE);
        for (int z = 0; z < 350; z++) {
            if (absorption) {
                double ab = Math.exp(-1 * aPlot.getValue(getNormalizedCoordinate(voxels[x][y][z]))/350);

                if (x == 87 && y == 87) {
                    System.out.println("+" + r);
                }

                r *= ab;
                g *= ab;
                b *= ab;
            }
            if (emission) {
                r += ePlot.getValue(getNormalizedCoordinate(voxels[x][y][z]), Model.ColorComponent.RED) / 350;
                if (x == 87 && y == 87)
                    System.out.println(r);
                g += ePlot.getValue(getNormalizedCoordinate(voxels[x][y][z]), Model.ColorComponent.GREEN) / 350;
                b += ePlot.getValue(getNormalizedCoordinate(voxels[x][y][z]), Model.ColorComponent.BLUE) / 350;
            }
            r = Math.max(0, Math.min(255, r));
            g = Math.max(0, Math.min(255, g));
            b = Math.max(0, Math.min(255, b));
        }
        return (int) r << 16 | (int) g << 8 | (int) b;
    }

    public void applyVisualization(BufferedImage srcImage, BufferedImage destImage, boolean emission, boolean absorption) {
        double[][][] voxels = new double[350][350][350];
        setVoxels(voxels);
        for (int j = 0; j < srcImage.getHeight(); j++) {
            for (int i = 0; i < srcImage.getWidth(); i++) {
                destImage.setRGB(i, j, processRay(i, j, srcImage.getRGB(i, j), absorption, emission, voxels));
            }
        }
    }

}
