package ru.nsu.fit.g16202.korshunov.ui.dialog;

import javax.swing.*;
import java.awt.*;

public class DitheringDialog extends Dialog {

    private JTextField tfRed;
    private JTextField tfGreen;
    private JTextField tfBlue;

    private boolean canceled = false;

    public DitheringDialog(Frame owner, boolean modal, String title) {
        super(owner,modal);
        setTitle(title);
        setLayout(new BorderLayout());
        JPanel panel = new JPanel();
        add(panel, BorderLayout.CENTER);
        panel.setLayout(new GridLayout(3,2));
        tfRed = new JTextField("2");
        tfGreen = new JTextField("2");
        tfBlue = new JTextField("2");
        tfRed.setPreferredSize(new Dimension(100, 20));
        tfGreen.setPreferredSize(new Dimension(100, 20));
        tfBlue.setPreferredSize(new Dimension(100, 20));
        panel.add(new JLabel("Nr:"));
        panel.add(tfRed);
        panel.add(new JLabel("Ng:"));
        panel.add(tfGreen);
        panel.add(new JLabel("Nb:"));
        panel.add(tfBlue);
        setLocationRelativeTo(owner);
        JPanel panel1 = new JPanel();
        JButton cancelButton = new JButton("Отмена");
        cancelButton.addActionListener(e -> {
            canceled = true;
            dispose();
        });
        panel1.setLayout(new FlowLayout());
        JButton okButton = new JButton("Ок");
        okButton.addActionListener(e -> {
            if (tfRed.getText().length() == 0 ||
            tfGreen.getText().length() == 0 ||
            tfBlue.getText().length() == 0) {
                showError();
                return;
            }
            try {
                if (Integer.valueOf(tfRed.getText()) < 2 || Integer.valueOf(tfRed.getText()) > 256) showError();
                if (Integer.valueOf(tfGreen.getText()) < 2 || Integer.valueOf(tfGreen.getText()) > 256) showError();
                if (Integer.valueOf(tfBlue.getText()) < 2 || Integer.valueOf(tfBlue.getText()) > 256) showError();
            } catch (NumberFormatException ex) {
                showError();
                return;
            }
            dispose();
        });
        panel1.add(okButton);
        panel1.add(cancelButton);
        add(panel1, BorderLayout.SOUTH);
        pack();
    }

    private void showError() {
        JOptionPane.showMessageDialog(this, "Введите числа");
    }

    public boolean wasCanceled() {
        return canceled;
    }

    public int getNr() {
        return Integer.valueOf(tfRed.getText());
    }

    public int getNg() {
        return Integer.valueOf(tfGreen.getText());
    }

    public int getNb() {
        return Integer.valueOf(tfBlue.getText());
    }

}
