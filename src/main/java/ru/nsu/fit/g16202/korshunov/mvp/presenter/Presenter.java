package ru.nsu.fit.g16202.korshunov.mvp.presenter;

import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.model.volume.VolumeVisualization;
import ru.nsu.fit.g16202.korshunov.mvp.view.View;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class Presenter {

    private Model model;
    private View view;
    private ImageReader imageReader = new ImageReader();
    private boolean emission = false;
    private boolean absorption = false;


    public Presenter(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void openFile() {
        BufferedImage img = imageReader.getImageFromFile(view);
        if (img != null) {
            view.clearImage(view.getImageA());
            view.clearImage(view.getImageB());
            view.clearImage(view.getImageC());
            view.setSavedImageA(img);
        }
        view.repaint();
    }

    public void saveFile() {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("BMP/PNG/JPEG", "bmp", "png", "jpg");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\FIT_16202_Korshunov_Life_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(view, "Сохранить изображение");

        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            try {
                ImageIO.write(view.getImageC(),"png",file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void applyRotatironFilter(double angle) {
        view.clearImage(view.getImageC());
        model.applyRotatironFilter(view.getImageB(), view.getImageC(), angle);
        view.repaint();
    }

    public View getView() {
        return view;
    }

    public void applyGrayScaleFilter() {
        model.applyGrayScaleFilter(view.getImageB(), view.getImageC());
        view.repaint();
    }

    public void newFile() {
        view.clearImage(view.getImageA());
        view.clearImage(view.getImageB());
        view.clearImage(view.getImageC());
        view.setSavedImageA(null);
        view.repaint();
    }

    public void applyNegativeFilter() {
        model.applyNegativeFilter(view.getImageB(), view.getImageC());
        view.repaint();
    }

    public void applyFloydDitheringFilter(int Nr, int Ng, int Nb) {
        model.applyFloydDitheringFilter(view.copyImage(view.getImageB()), view.getImageC(), Nr, Ng, Nb);
        view.repaint();
    }

    public void applyBlurFilter(int[][] matrix) {
        model.applyBlurFilter(view.getImageB(), view.getImageC(), matrix);
        view.repaint();
    }

    public void applySharpnessFilter(int[][] matrix) {
        model.applySharpnessFilter(view.getImageB(), view.getImageC(), matrix);
        view.repaint();
    }

    public void applyEmbossFilter(int[][] matrix) {
        model.applyEmbossFilter(view.getImageB(), view.getImageC(), matrix);
        view.repaint();
    }

    public void applyWaterColorFilter() {
        model.applyWaterColorFilter(view.getImageB(), view.getImageC());
        view.repaint();
    }

    public void applySobelFilter(int parameter) {
        model.applySobelFilter(view.getImageB(), view.getImageC(), parameter);
        view.repaint();
    }

    public void applyZoomFilter() {
        model.applyZoomFilter(view.getImageB(), view.getImageC());
        view.repaint();
    }

    public void applyGammaFilter(double parameter) {
        model.applyGammaFilter(view.getImageB(), view.getImageC(), parameter);
        view.repaint();
    }

    public void applyRobertsFilter(int parameter) {
        model.applyRobertsFilter(view.getImageB(), view.getImageC(), parameter);
        view.repaint();
    }

    public void applyOrderedDithering(int Nr, int Ng, int Nb) {
        model.applyOrderedDithering(view.getImageB(),view.getImageC(), Nr, Ng, Nb);
        view.repaint();
    }

    public void copyCtoB() {
        view.copyCtoB();
        view.repaint();
    }

    public void copyBtoC() {
        view.copyBtoC();
        view.repaint();
    }

    public void loadVolumeConfig() {
        final JFileChooser fc = new JFileChooser();
        FileFilter filter = new FileNameExtensionFilter("txt", "txt");
        fc.setFileFilter(filter);
        File dataDir = new File(".\\FIT_16202_Korshunov_Life_Data");
        if (!Files.exists(dataDir.toPath())) {
            try {
                Files.createDirectory(dataDir.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        fc.setCurrentDirectory(dataDir);
        int ret = fc.showDialog(view, "Открыть конфигурацию");

        if (ret == JFileChooser.APPROVE_OPTION) {
            try {
                model.loadVolumeVisualizationConfig(fc.getSelectedFile());
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(view, "Некорректный файл");
                return;
            }
        }
        view.drawAbsorptionPlot(VolumeVisualization.getAbsorptionPlot());
        view.drawEmissionPlot(VolumeVisualization.getEmissionPlot());
        view.repaint();
    }

    public void setEmission(boolean e) {
        emission = e;
    }

    public void setAbsorption(boolean a) {
        absorption = a;
    }

    public void applyVolumeRender() {
        model.applyVolumeRender(view.getImageB(), view.getImageC(), emission, absorption);
        view.repaint();
    }

    public void enableSelection(boolean b) {
        view.setAllowSelection(b);
    }

}
