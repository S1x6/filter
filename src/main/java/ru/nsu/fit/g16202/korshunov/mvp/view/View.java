package ru.nsu.fit.g16202.korshunov.mvp.view;

import javafx.util.Pair;
import ru.nsu.fit.g16202.korshunov.mvp.model.Model;
import ru.nsu.fit.g16202.korshunov.mvp.model.volume.AbsorptionPlot;
import ru.nsu.fit.g16202.korshunov.mvp.model.volume.EmissionPlot;
import ru.nsu.fit.g16202.korshunov.mvp.model.volume.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class View extends JPanel {

    private static final int VERTICAL_MARGIN = 10;
    private static final int HORIZONTAL_MARGIN = 10;

    private BufferedImage imageA;
    private BufferedImage imageB;
    private BufferedImage imageC;
    private BufferedImage savedImageA;
    private BufferedImage emissionPlotImage;
    private BufferedImage absorptionPlotImage;

    private boolean allowSelection;
    private int selectionX;
    private int selectionY;
    private int selectionWidth;
    private int selectionHeight;

    private boolean shouldSelect = true;

    public View() {
        super();

        setLayout(null);
        setBackground(Color.white);
        imageA = new BufferedImage(350, 350, BufferedImage.TYPE_INT_RGB);
        imageA.getGraphics().setColor(Color.WHITE);
        imageA.getGraphics().fillRect(0, 0, imageA.getWidth(), imageA.getHeight());
        imageB = new BufferedImage(350, 350, BufferedImage.TYPE_INT_RGB);
        imageB.getGraphics().setColor(Color.WHITE);
        imageB.getGraphics().fillRect(0, 0, imageB.getWidth(), imageB.getHeight());
        imageC = new BufferedImage(350, 350, BufferedImage.TYPE_INT_RGB);
        imageC.getGraphics().setColor(Color.WHITE);
        imageC.getGraphics().fillRect(0, 0, imageC.getWidth(), imageC.getHeight());

        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (allowSelection)
                    shouldSelect = true;
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                shouldSelect = false;
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        this.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (shouldSelect) {
                    makeSelection(e.getX(), e.getY());
                }
            }

            @Override
            public void mouseMoved(MouseEvent e) {
            }
        });
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(imageA, HORIZONTAL_MARGIN, VERTICAL_MARGIN, this);
        g.drawImage(imageB, HORIZONTAL_MARGIN * 2 + 350, VERTICAL_MARGIN, this);
        g.drawImage(imageC, HORIZONTAL_MARGIN * 3 + 700, VERTICAL_MARGIN, this);
        if (shouldSelect) {
            g.setXORMode(Color.WHITE);
            ((Graphics2D) g).setStroke(new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
                    BasicStroke.JOIN_MITER, 1.0f, new float[]{2f}, 0.0f));
            g.drawRect(selectionX, selectionY, selectionWidth, selectionHeight);
        }
        drawDashedBorder((Graphics2D) g, HORIZONTAL_MARGIN - 1, VERTICAL_MARGIN - 1, 351, 351);
        drawDashedBorder((Graphics2D) g, (HORIZONTAL_MARGIN - 1) * 2 + 351, VERTICAL_MARGIN - 1, 351, 351);
        drawDashedBorder((Graphics2D) g, (HORIZONTAL_MARGIN - 1) * 3 + 2 * 351, VERTICAL_MARGIN - 1, 351, 351);
        if (absorptionPlotImage != null) {
            g.drawImage(absorptionPlotImage, HORIZONTAL_MARGIN, VERTICAL_MARGIN * 2 + imageA.getHeight(), this);
        }
        if (emissionPlotImage != null) {
            g.drawImage(emissionPlotImage, HORIZONTAL_MARGIN * 2+ imageA.getWidth(), VERTICAL_MARGIN * 2 + imageA.getHeight(), this);
        }
    }

    private double getSelectionSquareWidthRatio(BufferedImage image) {
        double ratio;
        ratio = 350. / image.getWidth();
        if (ratio > 1.0) {
            ratio = 1.0;
        }
        return ratio;
    }

    private double getSelectionSquareHeightRatio(BufferedImage image) {
        double ratio;
        ratio = 350. / image.getHeight();
        if (ratio > 1.0) {
            ratio = 1.0;
        }
        return ratio;
    }

    public void makeSelection(int x, int y) {
        double widthRatio = getSelectionSquareWidthRatio(savedImageA);
        double heightRatio = getSelectionSquareHeightRatio(savedImageA);
        double minRation = Math.min(widthRatio, heightRatio);
        selectionWidth = savedImageA.getWidth() <= 350 ? (int) (savedImageA.getWidth() * minRation) : (int) (minRation * 350);
        selectionHeight = savedImageA.getHeight() <= 350 ? (int) (savedImageA.getHeight() * minRation) : (int) (minRation * 350);
        int maxW = (350 > savedImageA.getWidth() ? selectionWidth : 350);
        int maxH = (350 > savedImageA.getHeight() ? selectionHeight : 350);
        selectionX = x - HORIZONTAL_MARGIN - selectionWidth / 2 < 0 ? HORIZONTAL_MARGIN : HORIZONTAL_MARGIN + x - HORIZONTAL_MARGIN - selectionWidth / 2;
        if (selectionX > HORIZONTAL_MARGIN + maxW - selectionWidth) {
            selectionX = HORIZONTAL_MARGIN + maxW - selectionWidth;
        }
        if (selectionX - HORIZONTAL_MARGIN + selectionWidth > savedImageA.getWidth() * minRation) {
            selectionX = (int) (savedImageA.getWidth() * minRation - selectionWidth) + HORIZONTAL_MARGIN;
        }

        selectionY = y - VERTICAL_MARGIN - selectionHeight / 2 < 0 ? VERTICAL_MARGIN : VERTICAL_MARGIN + y - VERTICAL_MARGIN - selectionHeight / 2;
        if (selectionY > VERTICAL_MARGIN + maxH - selectionHeight) {
            selectionY = VERTICAL_MARGIN + maxH - selectionHeight;
        }
        //System.out.println(selectionY - VERTICAL_MARGIN + selectionHeight + " " + savedImageA.getHeight() * minRation);
        if (selectionY - VERTICAL_MARGIN + selectionHeight > savedImageA.getHeight() * minRation) {
            selectionY = (int) (savedImageA.getHeight() * minRation - selectionHeight) + VERTICAL_MARGIN;
        }
        int offsetX = (int) Math.round((selectionX - HORIZONTAL_MARGIN) / minRation);
        int offsetY = (int) Math.round((selectionY - VERTICAL_MARGIN) / minRation);
        try {
            imageB = savedImageA.getSubimage(offsetX, offsetY, maxW, maxH);
        } catch (java.awt.image.RasterFormatException ignored) {
            //System.out.println((offsetX + maxW - savedImageA.getWidth()) + " " + (offsetY + maxH - savedImageA.getHeight()));
            if (offsetX + maxW > savedImageA.getWidth()) {
                offsetX = savedImageA.getWidth() - maxW;
            }
            if (offsetY + maxH > savedImageA.getHeight()) {
                offsetY = savedImageA.getHeight() - maxH;
            }
            imageB = savedImageA.getSubimage(offsetX, offsetY, maxW, maxH);
        }
        repaint();
    }

    public void stopSelection() {
        shouldSelect = false;
    }

    private void drawDashedBorder(Graphics2D g, int x, int y, int w, int h) {
        Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{4}, 0);
        g.setColor(Color.black);
        g.setStroke(dashed);
        g.drawLine(x, y, x + w, y);
        g.drawLine(x + w, y, x + w, y + h);
        g.drawLine(x + w, y + h, x, y + h);
        g.drawLine(x, y + h, x, y);
    }

    public BufferedImage getImageA() {
        return imageA;
    }

    public BufferedImage getImageB() {
        return imageB;
    }

    public BufferedImage getImageC() {
        return imageC;
    }

    public void setSavedImageA(BufferedImage savedImageA) {
        this.savedImageA = savedImageA;
        stopSelection();
        if (savedImageA == null) {
            return;
        }
        Graphics2D g = imageA.createGraphics();
        Dimension desiredDim = getScaledDimension(
                new Dimension(savedImageA.getWidth(), savedImageA.getHeight()),
                new Dimension(imageA.getWidth(), imageA.getHeight())
        );
        g.drawImage(savedImageA, 0, 0, (int) desiredDim.getWidth(), (int) desiredDim.getHeight(), null);
        g.dispose();
    }

    public void clearImage(BufferedImage image) {
        Graphics2D g = image.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, image.getWidth(), image.getHeight());
        g.dispose();
    }

    private Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

        int original_width = imgSize.width;
        int original_height = imgSize.height;
        int bound_width = boundary.width;
        int bound_height = boundary.height;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        return new Dimension(new_width, new_height);
    }

    public void copyCtoB() {
        imageB = copyImage(imageC);
    }

    public void copyBtoC() {
        Graphics2D g = imageC.createGraphics();
        g.drawImage(imageB, 0, 0, imageC.getWidth(), imageC.getHeight(), this);
        g.dispose();
    }

    public BufferedImage copyImage(BufferedImage bi) {
        WritableRaster data = ((WritableRaster) bi.getData(new Rectangle(0, 0, bi.getWidth(), bi.getHeight()))).createWritableTranslatedChild(0, 0);
        return new BufferedImage(bi.getColorModel(), data, bi.isAlphaPremultiplied(), null);

    }

    public void drawAbsorptionPlot(AbsorptionPlot plot) {
        if (absorptionPlotImage == null) {
            absorptionPlotImage = new BufferedImage(301, 105, BufferedImage.TYPE_INT_RGB);
        }
        Graphics2D g = absorptionPlotImage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 301, 120);
        g.setStroke(new BasicStroke(2));
        g.setColor(Color.BLACK);
        g.drawLine(1, 1, 1, 104);
        g.drawLine(1, 104, 301, 104);
        g.setStroke(new BasicStroke(1));
        g.setColor(Color.BLACK);
        int doublePointNum = 0;
        Pair<Point, Point> doublePoint = !plot.getDoublePoints().isEmpty() ? plot.getDoublePoints().get(doublePointNum) : null;
        int lastX = 1;
        int lastY = 101 - (int) (plot.getValue(0) * 100);
        for (int i = 1; i < 100; i++) {
            if (doublePoint != null && doublePoint.getKey().getX() == i) {
                g.drawLine(lastX, lastY, lastX + 3, 101 - (int) (doublePoint.getKey().getY()*100));
                g.drawLine(lastX + 3, 101 - (int) (doublePoint.getKey().getY() * 100),
                        lastX + 3, 101 - (int) (doublePoint.getValue().getY() * 100));
                lastY = 101 - (int) (doublePoint.getValue().getY() * 100);
                lastX += 3;
                doublePointNum++;
                doublePoint = plot.getDoublePoints().size() > doublePointNum ? plot.getDoublePoints().get(doublePointNum) : null;
            } else {
                g.drawLine(lastX, lastY, lastX + 3, 101 - (int) (plot.getValue(i) * 100));
                lastX += 3;
                lastY = 101 - (int) (plot.getValue(i) * 100);
            }
        }
        g.dispose();

    }

    public void setAllowSelection(boolean allowSelection) {
        this.allowSelection = allowSelection;
    }

    @SuppressWarnings("Duplicates")
    public void drawEmissionPlot(EmissionPlot plot) {
        if (emissionPlotImage == null) {
            emissionPlotImage = new BufferedImage(310, 110, BufferedImage.TYPE_INT_RGB);
        }
        Graphics2D g = emissionPlotImage.createGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, 310, 110);
        g.setStroke(new BasicStroke(2));
        g.setColor(Color.BLACK);
        g.drawLine(1, 1, 1, 105);
        g.drawLine(1, 105, 301, 105);
        g.setStroke(new BasicStroke(1));
        g.setColor(Color.BLACK);
        drawOneComponentPlot(g, plot, Model.ColorComponent.RED);
        drawOneComponentPlot(g, plot, Model.ColorComponent.GREEN);
        drawOneComponentPlot(g, plot, Model.ColorComponent.BLUE);
        g.dispose();
    }

    private void drawOneComponentPlot(Graphics2D g, EmissionPlot plot, Model.ColorComponent component) {
        int offset = 0;
        switch (component) {
            case RED:
                offset = 1;
                g.setColor(Color.RED);
                break;
            case GREEN:
                offset = 2;
                g.setColor(Color.GREEN);
                break;
            case BLUE:
                offset = 3;
                g.setColor(Color.BLUE);
                break;
        }
        int doublePointNum = 0;
        Pair<Point, Point> doublePoint;
        doublePoint = getPointPointPair(plot, component, doublePointNum);
        int lastX = 1 + offset;
        int lastY = 104 - (int) (plot.getValue(0, component) * 100/255) - offset;
        for (int i = 1; i < 100; i++) {
            if (doublePoint != null && doublePoint.getKey().getX() == i) {
                g.drawLine(lastX, lastY, lastX + 3, 104 - (int) (doublePoint.getKey().getY() * 100/255) - offset);
                g.drawLine(lastX + 3, 104 - (int) (doublePoint.getKey().getY() * 100/255) - offset,
                        lastX + 3, 104 - (int) (doublePoint.getValue().getY() * 100/255) - offset);
                lastY = 104 - (int) (doublePoint.getValue().getY() * 100/255) - offset;
                lastX += 3;
                doublePointNum++;
                switch (component) {
                    case RED:
                        doublePoint = plot.getDoublePointsR().size() > doublePointNum ? plot.getDoublePointsR().get(doublePointNum) : null;
                        break;
                    case GREEN:
                        doublePoint = plot.getDoublePointsG().size() > doublePointNum ? plot.getDoublePointsG().get(doublePointNum) : null;
                        break;
                    case BLUE:
                        doublePoint = plot.getDoublePointsB().size() > doublePointNum ? plot.getDoublePointsB().get(doublePointNum) : null;
                }
            } else {
                g.drawLine(lastX, lastY, lastX + 3, 104 - (int) (plot.getValue(i, component) * 100/255) - offset);
                lastX += 3;
                lastY = 104 - (int) (plot.getValue(i, component) * 100/255) - offset;
            }
        }
    }

    private Pair<Point, Point> getPointPointPair(EmissionPlot plot, Model.ColorComponent component, int doublePointNum) {
        switch (component) {
            case RED:
                return !plot.getDoublePointsR().isEmpty() ? plot.getDoublePointsR().get(doublePointNum) : null;
            case GREEN:
                return !plot.getDoublePointsG().isEmpty() ? plot.getDoublePointsG().get(doublePointNum) : null;
            case BLUE:
                return !plot.getDoublePointsB().isEmpty() ? plot.getDoublePointsB().get(doublePointNum) : null;
        }
        return null;
    }
}
