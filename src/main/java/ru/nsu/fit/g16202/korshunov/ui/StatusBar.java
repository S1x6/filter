package ru.nsu.fit.g16202.korshunov.ui;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;

public class StatusBar extends JPanel {

    private JLabel status;

    public StatusBar(Container parent) {
        super();
        status = new JLabel();
        this.setBorder(new BevelBorder(BevelBorder.LOWERED));
        this.setPreferredSize(new Dimension(parent.getWidth(), 16));
        this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        status = new JLabel();
        status.setHorizontalAlignment(SwingConstants.LEFT);
        add(status);
    }

    public void setStatus(String status) {
        this.status.setText(status);
    }

}
