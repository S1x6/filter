package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;

public class ZoomFilter {

    public void applyZoomFilter(BufferedImage srcImage, BufferedImage destImage) {
        makeZoom(srcImage, 88, 88, destImage, Model.ColorComponent.RED);
        makeZoom(srcImage, 88, 88, destImage, Model.ColorComponent.GREEN);
        makeZoom(srcImage, 88, 88, destImage, Model.ColorComponent.BLUE);
    }

    private void makeZoom(BufferedImage smallImage, int startX, int startY, BufferedImage bigImage, Model.ColorComponent component) {
        int smallX;
        int smallY = startY;
        for (int j = 0; j < 350; j += 2) {
            smallX = startX;
            for (int i = 0; i < 350; i++) {
                if (i % 2 == 0) {
                    ModelUtils.setComponent(bigImage, i, j, ModelUtils.getComponent(
                            smallImage.getRGB(smallX, smallY), component), component);
                    smallX++;
                } else {
                    int newColor = (ModelUtils.getComponent(smallImage.getRGB(smallX - 1, smallY), component) +
                            ModelUtils.getComponent(smallImage.getRGB(smallX, smallY), component)) / 2;
                    ModelUtils.setComponent(bigImage, i, j, newColor, component);
                }
            }
            smallY++;
        }
        for (int j = 1; j < 349; j += 2) {
            for (int i = 0; i < 350; i++) {
                int newColor = (ModelUtils.getComponent(bigImage.getRGB(i, j - 1), component) +
                        ModelUtils.getComponent(bigImage.getRGB(i, j + 1), component)) / 2;
                ModelUtils.setComponent(bigImage, i, j, newColor, component);
            }
        }
        for (int i = 0; i < 350; i++) {
            int newColor = (ModelUtils.getComponent(bigImage.getRGB(i, 348), component));
            ModelUtils.setComponent(bigImage, i, 349, newColor, component);
        }
    }
}
