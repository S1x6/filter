package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.*;
import java.awt.image.BufferedImage;

class DitheringFilter {

    private Model model;

    DitheringFilter(Model model) {
        this.model = model;
    }

    void applyFloydDitheringFilter(BufferedImage srcImage, BufferedImage destImage, int Nr, int Ng, int Nb) {
        Graphics2D g = destImage.createGraphics();
        g.drawImage(srcImage, 0, 0, destImage.getWidth(), destImage.getHeight(), null);
        g.dispose();
        doFloydDithering(destImage, Nr, Model.ColorComponent.RED);
        doFloydDithering(destImage, Ng, Model.ColorComponent.GREEN);
        doFloydDithering(destImage, Nb, Model.ColorComponent.BLUE);
    }

    private void doFloydDithering(BufferedImage image, int N, Model.ColorComponent component) {
        for (int j = 0; j < image.getHeight(); ++j) { //для каждого пикселя
            for (int i = 0; i < image.getWidth(); ++i) {
                int oldComponent = ModelUtils.getComponent(image.getRGB(i, j), component);
                int newComponent = ModelUtils.getNearestColorComponent(oldComponent, N);
                ModelUtils.setComponent(image, i, j, newComponent, component);
                double quantError = oldComponent - newComponent;
                if (i + 1 < image.getWidth()) {
                    int errorComponent = (int) (ModelUtils.getComponent(image.getRGB(i + 1, j), component) + quantError * 7 / 16);
                    errorComponent = Math.min(255, Math.max(0, errorComponent));
                    ModelUtils.setComponent(image, i + 1, j, errorComponent, component);
                }
                if (i - 1 >= 0 && j + 1 < image.getHeight()) {
                    int errorComponent = (int) (ModelUtils.getComponent(image.getRGB(i - 1, j + 1), component) + quantError * 3 / 16);
                    errorComponent = Math.min(255, Math.max(0, errorComponent));
                    ModelUtils.setComponent(image, i - 1, j+1, errorComponent, component);
                }
                if (j + 1 < image.getHeight()) {
                    int errorComponent = (int) (ModelUtils.getComponent(image.getRGB(i, j+1), component) + quantError * 5 / 16);
                    errorComponent = Math.min(255, Math.max(0, errorComponent));
                    ModelUtils.setComponent(image, i, j+1, errorComponent, component);
                }
                if (j + 1 < image.getHeight() && i + 1 < image.getWidth()) {
                    int errorComponent = (int) (ModelUtils.getComponent(image.getRGB(i + 1, j+1), component) + quantError * 1 / 16);
                    errorComponent = Math.min(255, Math.max(0, errorComponent));
                    ModelUtils.setComponent(image, i + 1, j+1, errorComponent, component);
                }
            }
        }
    }

    void applyOrderedDithering(BufferedImage srcImage, BufferedImage destImage, int Nr, int Ng, int Nb) {
        int m[][] = getMatrix();
        applyOrderedDitheringToChannel(srcImage, destImage, m, Nr, Model.ColorComponent.RED);
        applyOrderedDitheringToChannel(srcImage, destImage, m, Ng, Model.ColorComponent.GREEN);
        applyOrderedDitheringToChannel(srcImage, destImage, m, Nb, Model.ColorComponent.BLUE);
    }

    private void applyOrderedDitheringToChannel(BufferedImage srcImage, BufferedImage destImage, int[][] m, int N, Model.ColorComponent component) {
        int x, y;
        for (int j = 0; j < srcImage.getHeight(); ++j) { //для каждого пикселя
            for (int i = 0; i < srcImage.getWidth(); ++i) {
                x = i % 16;
                y = j % 16;
                int comp = Math.min(Math.max(ModelUtils.getComponent(srcImage.getRGB(i, j), component) + (m[x][y] - 128) / (N - 1), 0), 255);
                int color = ModelUtils.getNearestColorComponent(comp, N);
                ModelUtils.setComponent(destImage, i, j, color, component);
            }
        }
    }

    private int[][] getMatrix() {
        int[][] m = new int[16][16];
        for (int y = 0; y < 16; ++y) {
            System.out.println(" ");
            for (int x = 0; x < 16; ++x) {
                int v = 0, mask = 3, xc = x ^ y, yc = y;
                for (int bit = 0; bit < 16; --mask) {
                    v |= ((yc >> mask) & 1) << bit++;
                    v |= ((xc >> mask) & 1) << bit++;
                }
                m[x][y] = v;
            }
        }
        return m;
    }

}
