package ru.nsu.fit.g16202.korshunov.mvp.model;

import java.awt.image.BufferedImage;

class ColorFilter {
    void applyGrayScaleFilter(BufferedImage srcImage, BufferedImage destImg) {
        for (int j = 0; j < srcImage.getHeight(); ++j) {
            for (int i = 0; i < srcImage.getWidth(); ++i) {
                int color = srcImage.getRGB(i, j);
                int r = (int) (ModelUtils.getRedComponent(color) * 0.299);
                int g = (int) (ModelUtils.getGreenComponent(color) * 0.587);
                int b = (int) (ModelUtils.getBlueComponent(color) * 0.114);
                int av = r + g + b;
                destImg.setRGB(i, j, av << 16 | av << 8 | av);
            }
        }
    }

    void applyEqualgrayScaleFilter(BufferedImage srcImage, BufferedImage destImage) {
        for (int j = 0; j < srcImage.getHeight(); ++j) {
            for (int i = 0; i < srcImage.getWidth(); ++i) {
                int color = srcImage.getRGB(i, j);
                int r = (ModelUtils.getRedComponent(color));
                int g = (ModelUtils.getGreenComponent(color));
                int b = (ModelUtils.getBlueComponent(color));
                int av = (r + g + b) / 3;
                destImage.setRGB(i, j, av << 16 | av << 8 | av);
            }
        }
    }

    void applyNegativeFilter(BufferedImage srcImage, BufferedImage destImage) {
        for (int j = 0; j < srcImage.getHeight(); ++j) {
            for (int i = 0; i < srcImage.getWidth(); ++i) {
                int color = srcImage.getRGB(i, j);
                int r = 255 - ModelUtils.getRedComponent(color);
                int b = 255 - ModelUtils.getGreenComponent(color);
                int g = 255 - ModelUtils.getBlueComponent(color);
                destImage.setRGB(i, j, r << 16 | g << 8 | b);
            }
        }
    }
}
